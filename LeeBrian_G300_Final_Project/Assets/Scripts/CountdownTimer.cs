﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour {
    float currentTime = 0f;
    float startingTime = 60f;

    [SerializeField] Text countdownText;

	// Use this for initialization
	void Start () {
        currentTime = startingTime;
	}

    // Update is called once per frame
    void Update() {
        currentTime -= 1 * Time.deltaTime;
        countdownText.text = "Escape " + currentTime.ToString("F") + "s";

        if(currentTime <= 30)
        {
            countdownText.color = Color.yellow;
        }
        if(currentTime <= 10)
        {
            countdownText.color = Color.red;
        }

        if (currentTime <= 0)
        {
            countdownText.text = "Mission Failed";
            return;
        }
	}
}
