﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathTrigger : MonoBehaviour {

    SystemControl systemControl;    //call in a instance of the systemControl object

    void Start () {
        systemControl = GameObject.FindGameObjectWithTag("SystemController").GetComponent<SystemControl>(); //Finding object that is currently the SystemController
    }
	
	void Update () {
		
	}

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject == systemControl.controlledUnit.gameObject)   //if object touching is the current systemController
        {
            SceneManager.LoadScene("Main"); //Load main
        }
    }
}