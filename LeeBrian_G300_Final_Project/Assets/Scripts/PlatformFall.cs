﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformFall : MonoBehaviour {

    SystemControl systemControl; //trying
    public float fallDelay = 1f;

    private Rigidbody2D rb2d;

    void Start()
    {
        systemControl = GameObject.FindGameObjectWithTag("SystemController").GetComponent<SystemControl>(); //Finding object that is currently the SystemController, TEST
    }
    void Awake ()
    {
        rb2d = GetComponent<Rigidbody2D>();
	}
	
    void OnCollisionEnter2D(Collision2D other)
    {
        //print(other.gameObject.name + systemControl.controlledUnit.name + "hi");
        if (other.gameObject == systemControl.controlledUnit.gameObject)
        {   //ORIGINAL (other.gameObject.CompareTag("Player"))
            Invoke("Fall", fallDelay);
            //print("yeah");
        }
    }

    void Fall()
    {
        rb2d.isKinematic = false;
    }
}
