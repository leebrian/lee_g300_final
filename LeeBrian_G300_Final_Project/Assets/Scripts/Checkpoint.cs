﻿using UnityEngine;
using System.Collections;
public class CheckPoint : MonoBehaviour
{
    // have we been triggered?
    bool triggered;
    SystemControl systemControl;    //call in a instance of the systemControl object

    void Awake()
    {
        triggered = false;
        systemControl = GameObject.FindGameObjectWithTag("SystemController").GetComponent<SystemControl>(); //Finding object that is currently the SystemController
    }
    // called whenever another collider enters our zone (if layers match)
    void OnTriggerEnter2D(Collider2D other)
    {
        // check we haven't been triggered yet!
        if (!triggered)
        {
            // check we actually collided with 
            // a character. It would be best to
            // setup your layers so this check is
            // not required, by creating a layer 
            // "Checkpoint" that will only collide 
            // with characters.

            /*if (collider.gameObject.layer
                == LayerMask.NameToLayer("Character"))
            {*/
            if (other.gameObject == systemControl.controlledUnit.gameObject)   //if object touching is the current systemController
            {
                Trigger();
            }
        }
    }
    void Trigger()
    {
        // Tell the animation controller about our 
        // recent triggering
        GetComponent<Animator>().SetTrigger("Triggered");
        triggered = true;
    }
}