﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    [HideInInspector] public bool facingRight = true;   //start game facing right
    [HideInInspector] public bool jump = false;         //don't start the game jumping

    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;
    public static bool playerNearSeat;
    public bool controlled = false;

    private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;

	// Use this for initialization
	void Awake ()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(groundCheck != null)
            grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (controlled == false)
            return;

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            jump = true;
        }
	}

    void FixedUpdate()
    {
        if (controlled == false)
            return;

        float h = Input.GetAxis("Horizontal");
        
        if (anim != null)
            anim.SetFloat("Speed", Mathf.Abs(h));

        if(h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (h > 0 && !facingRight)
            Flip();

        else if (h < 0 && facingRight)
            Flip();

        if(jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false; //prevents double(infinte) jump
        }
    }

    void Flip()
    {
        facingRight = !facingRight;                     //switches direction
        Vector3 theScale = transform.localScale;        //checks local transform controls?
        theScale.x *= -1;                               //reverses value(direction)
        transform.localScale = theScale;                //sets new change to current change
    }
}
