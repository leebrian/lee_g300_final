﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndText : MonoBehaviour {
    [SerializeField] Text timer;
    [SerializeField] Text endcard;

	// Use this for initialization
	void Start () {

	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        timer.gameObject.SetActive(false);
        endcard.gameObject.SetActive(true);
        print("hello");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
